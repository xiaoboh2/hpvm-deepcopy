set -e
set -o xtrace
#cd build
#hpvm-clang -DDEVICE=GPU_TARGET --hetero-cc --hpvm-target gpu -I/home/xiaoboh2/hpvm/hpvm/benchmarks/include  --verbose -fno-exceptions ../src/$1.cc  main.gpu && ./main.gpu

/home/xiaoboh2/hpvm/hpvm/build/bin/clang++ -fPIC -c ../src/hpvm_dclib.cpp -o hpvm_dclib.o

/home/xiaoboh2/hpvm/hpvm/build/bin/clang++ -O1 -I../src -I/home/xiaoboh2/hpvm/hpvm/benchmarks/include -I/home/xiaoboh2/hpvm/hpvm/build/include -I/home/xiaoboh2/hpvm/hpvm/llvm/include -I/home/xiaoboh2/hpvm/hpvm/llvm/tools/hpvm/./include -I/home/xiaoboh2/hpvm/hpvm/build/tools/hpvm -I/home/xiaoboh2/hpvm/hpvm/llvm/tools/hpvm/projects/hpvm-tensor-rt/./tensor_runtime/include -I/home/xiaoboh2/hpvm/hpvm/build/tools/hpvm/projects/hpvm-tensor-rt/tensor_runtime/include -I/usr/local/cuda/targets/x86_64-linux/include -I/usr/include -fno-exceptions -DDEVICE=GPU_TARGET ../src/tests/$1 -E -o $1.deepcopy.cc

/home/xiaoboh2/hpvm/hpvm/build/bin/hetero-dc $1.deepcopy.cc

/home/xiaoboh2/hpvm/hpvm/build/bin/clang++ -O1 -I../src -I/home/xiaoboh2/hpvm/hpvm/benchmarks/include -I/home/xiaoboh2/hpvm/hpvm/build/include -I/home/xiaoboh2/hpvm/hpvm/llvm/include -I/home/xiaoboh2/hpvm/hpvm/llvm/tools/hpvm/./include -I/home/xiaoboh2/hpvm/hpvm/build/tools/hpvm -I/home/xiaoboh2/hpvm/hpvm/llvm/tools/hpvm/projects/hpvm-tensor-rt/./tensor_runtime/include -I/home/xiaoboh2/hpvm/hpvm/build/tools/hpvm/projects/hpvm-tensor-rt/tensor_runtime/include -I/usr/local/cuda/targets/x86_64-linux/include -I/usr/include -fno-exceptions -DDEVICE=GPU_TARGET -emit-llvm -S $1.deepcopy.cc -o $1.hetero.ll

/home/xiaoboh2/hpvm/hpvm/build/bin/hcc $1.hetero.ll -declsfile /home/xiaoboh2/hpvm/hpvm/build/tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -sanitise-funcs -S -o $1.ll

/home/xiaoboh2/hpvm/hpvm/build/bin/opt -enable-new-pm=0 -load HPVMGenHPVM.so -genhpvm -globaldce -S $1.ll -o $1.hpvm.ll
/home/xiaoboh2/hpvm/hpvm/build/bin/opt -enable-new-pm=0 -load HPVMBuildDFG.so -load HPVMLocalMem.so -load HPVMDFG2LLVM_GPU_OCL.so -load HPVMDFG2LLVM_CPU.so -load HPVMClearDFG.so -buildDFG -localmem -dfg2llvm-gpu-ocl -dfg2llvm-cpu -clearDFG -S $1.hpvm.ll -o $1.llvm.ll
/home/xiaoboh2/hpvm/hpvm/build/bin/llvm-link $1.llvm.ll /home/xiaoboh2/hpvm/hpvm/build/tools/hpvm/projects/hpvm-rt/hpvm-rt.bc -S -o $1.linked.bc

# Run time linking no longer necessary. Run time module included in hpvm-rt
#/home/xiaoboh2/hpvm/hpvm/build/bin/clang++ $1.linked.bc hpvm_dclib.o -o main.gpu -L/home/xiaoboh2/hpvm/hpvm/build/lib -Wl,-rpath=/home/xiaoboh2/hpvm/hpvm/build/lib -L/usr/local/cuda/lib64 -Wl,-rpath=/usr/local/cuda/lib64 -lpthread -l:libOpenCL.so 
/home/xiaoboh2/hpvm/hpvm/build/bin/clang++ $1.linked.bc -o main.gpu -L/home/xiaoboh2/hpvm/hpvm/build/lib -Wl,-rpath=/home/xiaoboh2/hpvm/hpvm/build/lib -L/usr/local/cuda/lib64 -Wl,-rpath=/usr/local/cuda/lib64 -lpthread -l:libOpenCL.so 
/home/xiaoboh2/hpvm/hpvm/build/bin/llvm-ocl $1.hpvm.kernels.ll
#cd -
