#include <sstream>
#include <string>
#include <unordered_set>
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Driver/Options.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

using namespace clang;
using namespace llvm;
using namespace clang::tooling;

std::string decl2str(clang::Decl *d, ASTContext *ctx) {
  // (T, U) => "T,,"
  auto &sm = ctx->getSourceManager();
  std::string text =
      Lexer::getSourceText(CharSourceRange::getTokenRange(d->getSourceRange()),
                           sm, LangOptions(), 0)
          .str();
  if (text.size() > 0 && (text.at(text.size() - 1) == ','))
    // the text can be "" return
    Lexer::getSourceText(CharSourceRange::getCharRange(d->getSourceRange()), sm,
                         LangOptions(), 0);
  return text;
}

class FindNamedCallVisitor : public RecursiveASTVisitor<FindNamedCallVisitor> {
public:
  explicit FindNamedCallVisitor(ASTContext *Context)
      : Context(Context),
        rewriter(Context->getSourceManager(), Context->getLangOpts()),
        print_policy(Context->getLangOpts()) {
    print_policy.FullyQualifiedName = 1;
    print_policy.SuppressScope = 0;
    print_policy.PrintCanonicalTypes = 1;
  }

  bool VisitStmt(Stmt *st) {
    if (CallExpr *CallExpression = dyn_cast<CallExpr>(st)) {
      QualType q = CallExpression->getType();
      const clang::Type *t = q.getTypePtrOrNull();

      if (t != NULL) {
        if (FunctionDecl *func = CallExpression->getDirectCallee()) {
          const std::string funcName = func->getNameInfo().getAsString();
          if (funcName == "hpvm_do_snapshot") {
            FullSourceLoc FullLocation =
                Context->getFullLoc(CallExpression->getBeginLoc());
            if (FullLocation.isValid())
              llvm::outs() << "Found snapshot call " << funcName << " at "
                           << FullLocation.getSpellingLineNumber() << ":"
                           << FullLocation.getSpellingColumnNumber() << "\n";
            addDecleatation(func->getParamDecl(0)
                                ->getType()
                                .getTypePtrOrNull()
                                ->getPointeeCXXRecordDecl());
          }
        }
      }
    }
    return true;
  }

  void addDecleatation(const CXXRecordDecl *Declaration) {
    // Skip null and unknown definition
    if (!Declaration || !Declaration->hasDefinition())
      return;

    // Skip visited ones
    CXXRecordDecl *definition = Declaration->getDefinition();
    if (definitions.count(definition))
      return;
    definitions.insert(definition);

    // Debug prints
    FullSourceLoc FullLocation = Context->getFullLoc(definition->getBeginLoc());
    if (FullLocation.isValid()) {
      llvm::outs() << "Found definition " << fullTemplateName(definition)
                   << " at " << FullLocation.getSpellingLineNumber() << ":"
                   << FullLocation.getSpellingColumnNumber() << "\n";
      // definition->dumpColor();
    }
    if (isa<ClassTemplateSpecializationDecl>(definition)) {
      llvm::outs() << "\t Template specialization! \n";
    }

    // Add each field recursively
    for (FieldDecl *f : definition->fields()) {
      llvm::outs() << "\t Field: " << f->getName() << "\n";
      if (const clang::Type *memberType = f->getType().getTypePtrOrNull()) {
        if (memberType->isPointerType())
          addDecleatation(memberType->getPointeeCXXRecordDecl());
        else
          addDecleatation(memberType->getAsCXXRecordDecl());
      }
    }

    // Add each base class recursively
    for (auto base_ty : definition->bases()) {
      auto base = base_ty.getType()->getAsCXXRecordDecl();
      addDecleatation(base);
    }
  }

  // bool VisitCXXRecordDecl(CXXRecordDecl *Declaration) {}
  std::string fullTemplateName(CXXRecordDecl *def) {
    std::string str;
    raw_string_ostream oss(str);
    if (isa<ClassTemplateSpecializationDecl>(def)) {
      if (auto *tsd = cast<ClassTemplateSpecializationDecl>(def)) {
        tsd->getNameForDiagnostic(oss, print_policy, true);
      }
    } else
      oss << def->getName().str();
    return oss.str();
  }

  std::string getTypeSpecificBehaviorName(CXXRecordDecl *def) {
    return std::string("hpvm_snapshot_custom_behavior_") +
           std::to_string(def->getLocation().getHashValue());
  }

  bool shouldGenerate(CXXRecordDecl *definition) {
    if (generated_class_loc.count(definition->getLocation())) {
      llvm::outs() << "Skip " << fullTemplateName(definition)
                   << ", already generated\n";
      return false;
    }
    if (definition->fields().empty() && (definition->bases().empty())) {
      llvm::outs() << "Skip " << fullTemplateName(definition)
                   << ", empty class\n";
      return false;
    }
    if (definition->getName().size() == 0) {
      llvm::outs() << "Skip " << fullTemplateName(definition)
                   << ", no name class\n";
      return false;
    }
    std::string def_txt = decl2str(definition, Context);
    if (def_txt.find("{") == std::string::npos) {
      llvm::outs() << "Skip " << fullTemplateName(definition)
                   << ", no body class\n";
      llvm::outs() << "Text form: \n" << def_txt << "\n";
      return false;
    }
    return true;
  }

  void doDcTransformOn(CXXRecordDecl *definition) {
    //  llvm::outs() << "Text Form: \n" << def_txt << "\n";
    //  definition->dumpColor();
    if (!shouldGenerate(definition))
      return;
    generated_class_loc.insert(definition->getLocation());
    llvm::outs() << "Generating dc for " << fullTemplateName(definition)
                 << "\n";

    std::ostringstream oss;
    // Add compile time shorthand for the current type
    oss << "#define DEFINE_SELF "
        << "struct _self_type_tag {}; "
        << "constexpr auto _self_type_helper() -> "
           "decltype(::SelfType::Writer<_self_type_tag, decltype(this)>{}, "
           "void()) {}"
        << "using Self = ::SelfType::Read<_self_type_tag>"
        << "\n";
    oss << "DEFINE_SELF;\n";

    // First insert the type specific behavior
    SourceLocation func_loc = definition->getEndLoc();
    std::string class_name = fullTemplateName(definition);
    oss << "protected:\n";
    oss << "inline void " << getTypeSpecificBehaviorName(definition)
        << "(HpvmBuf& buf,"
        << "Self* dst, "
        << "Self* src){\n";

    // Generate behavior for this particular class (exclude base class)
    for (FieldDecl *f : definition->fields()) {
      llvm::outs() << "\tField: " << f->getName() << "\n";
      std::string fname = f->getName().str();
      if (f->getType()->isPointerType())
        oss << "\tsnapshot_pointer(buf, dst->" << fname << ", src->" << fname
            << ");\n";
      else
        oss << "\thpvm_snapshot_custom(buf, &dst->" << fname << ", &src->"
            << fname << ");\n";
    }
    oss << "#ifdef ADC_DEBUG\n";
    oss << "\tstd::cout<<\"" << getTypeSpecificBehaviorName(definition) << "\""
        << "<<std::endl;\n";
    oss << "#endif\n";
    oss << "}\n";

    // Generate behavior for this class and all base_classes
    oss << "public:\n";
    oss << "inline void hpvm_snapshot_custom_behavior(HpvmBuf& buf,"
        << "Self* dst, "
        << "Self* src){\n";

    oss << "\t" << getTypeSpecificBehaviorName(definition)
        << "(buf, dst,src );";
    for (auto base_ty : definition->bases()) {
      auto base = base_ty.getType()->getAsCXXRecordDecl();
      if (!shouldGenerate(base))
        continue;
      // TODO skip base type without a body, and call base of base instead
      llvm::outs() << "\tBase: " << fullTemplateName(base) << "\n";
      std::string base_name = fullTemplateName(base);
      oss << "\nthis->" << getTypeSpecificBehaviorName(base)
          << "(buf, dst,src );\n";
    }
    oss << "}\n";

    rewriter.InsertTextAfter(func_loc, oss.str());
  }

  void commitChanges() {
    for (auto *def : definitions)
      doDcTransformOn(def);
    rewriter.overwriteChangedFiles();
  }

private:
  std::set<SourceLocation>
      generated_class_loc; // remove duplicate specialization
  std::set<CXXRecordDecl *> definitions;
  ASTContext *Context;
  Rewriter rewriter;
  PrintingPolicy print_policy;
};

class FindNamedCallConsumer : public clang::ASTConsumer {
public:
  explicit FindNamedCallConsumer(ASTContext *Context) : Visitor(Context) {}

  virtual void HandleTranslationUnit(clang::ASTContext &Context) {
    Visitor.TraverseDecl(Context.getTranslationUnitDecl());
    Visitor.commitChanges();
  }

private:
  FindNamedCallVisitor Visitor;
};

class FindNamedCallAction : public clang::ASTFrontendAction {
public:
  FindNamedCallAction() {}

  virtual std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
    return std::unique_ptr<clang::ASTConsumer>(
        new FindNamedCallConsumer(&Compiler.getASTContext()));
  }
};

static llvm::cl::OptionCategory MyToolCategory("hetero-dc options");

int main(int argc, const char **argv) {
  auto OptionsParser = CommonOptionsParser::create(argc, argv, MyToolCategory);
  llvm::consumeError(OptionsParser.takeError());

  ClangTool Tool(OptionsParser->getCompilations(),
                 OptionsParser->getSourcePathList());

  int result = Tool.run(newFrontendActionFactory<FindNamedCallAction>().get());
  return result;
}
