#include <typeinfo>
#include <unordered_map>

extern "C" {
// In a separate file ----------------------
// TODO: move to separate file to make this global
#ifndef HPVM_ALLOCATION_RECORD
#define HPVM_ALLOCATION_RECORD
std::unordered_map<void*, std::pair<const std::type_info*, size_t>>
    hpvm_allocation_record;
#endif
};
