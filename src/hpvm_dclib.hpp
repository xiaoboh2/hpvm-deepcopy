#pragma once
#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
// #include <tr2/type_traits> // Broken:
//  https://llvm.org/bugs/show_bug.cgi?id=23915
#include <iostream>
#include <limits>
#include <typeinfo>
#include <unordered_map>

// necessary to prevent exception in itlib
#define ITLIB_SMALL_VECTOR_ERROR_HANDLING ITLIB_SMALL_VECTOR_ERROR_HANDLING_NONE
#define ITLIB_SMALL_VECTOR_NO_DEBUG_BOUNDS_CHECK
#define ITLIB_FLAT_MAP_NO_THROW

// Whatever library we use in the library, it could not be deep copied
#include "./itlib/include/itlib/flat_map.hpp"
#include "./itlib/include/itlib/small_vector.hpp"
#include "./pinnedVector/pinnedVector/PinnedVector.h"

// [cpp obj {ptr 1, ptr2}, ptr1{ptr3}.. ] -> Buffer[ cpp obj{ptr->relative
// address}, *ptr1, *ptr2  ] Library:
//  memory_map: tracking allocation type/size/meta_info
//  Helper functions: do buffer management
// Codegen:
//  Enumerate member, call library
// User code:
//  Tell library/Codegen we want to serialize object

using byte = char;

// In a separate file ----------------------
// TODO: move to separate file to make this global
// #ifndef HPVM_ALLOCATION_RECORD
// #define HPVM_ALLOCATION_RECORD
// static std::unordered_map<void*, std::pair<const std::type_info*, size_t>>
// hpvm_allocation_record;
// #endif

// Lib header provides ---------------------
extern std::unordered_map<void*, std::pair<const std::type_info*, size_t>>
    hpvm_allocation_record;

bool is_allocated_memory(void* ptr) {
  return hpvm_allocation_record.count(ptr);
}
size_t allocation_size(void* ptr) {
  if (is_allocated_memory(ptr)) return hpvm_allocation_record[ptr].second;
  return 0;
}

void* hpvm_malloc(size_t sz) {
  void* ptr = malloc(sz);
  hpvm_allocation_record[ptr] = {&typeid(void), sz};
  std::cout << "[dc_lib] malloc " << sz << " bytes at " << ptr << "\n";
  return ptr;
}

void hpvm_free(void* ptr) {
  std::cout << "[dc_lib] free " << ptr << "\n";
  free(ptr);
  hpvm_allocation_record.erase(ptr);
}

template <class T, class... Args>
T* hpvm_new(Args... args) {
  T* obj = new T(args...);
  hpvm_allocation_record[obj] = {&typeid(T), sizeof(T)};
  return obj;
}

template <class T>
T* hpvm_new_arr(size_t num_obj) {
  T* obj = new T[num_obj];
  hpvm_allocation_record[obj] = {&typeid(T), num_obj * sizeof(T)};
  return obj;
}

template <class T, class... Args>
void hpvm_delete(T* obj) {
  hpvm_allocation_record.erase(obj);
  delete obj;
}
template <class T, class... Args>
void hpvm_delete_arr(T* obj) {
  hpvm_allocation_record.erase(obj);
  delete[] obj;
}

// Hpvm Buf layout:
// [header, pointer_recover_list(size_t[]), obj_buf]
class HpvmBufHeader {
 public:
  size_t pointer_recover_list_size;
  size_t obj_size;
  byte buf[];

  template <class T = void>
  T* get_obj_start() {
    return (T*)(buf + pointer_recover_list_size * sizeof(size_t));
  }
  size_t* get_pointer_list_start() { return (size_t*)buf; }
  size_t* get_pointer_at(size_t offset) {
    size_t* sub_ptr = (size_t*)(((char*)get_obj_start()) + offset);
    return sub_ptr;
  }
  void to_relative_pointer(void* base) {
    for (int i = 0; i < pointer_recover_list_size; i++) {
      size_t pointer_offset = get_pointer_list_start()[i];
      size_t* sub_ptr = get_pointer_at(pointer_offset);
#ifndef USE_HPVM
      std::cout << "[dc_lib] "
                << "To relative: " << (void*)sub_ptr[0];
#endif
      sub_ptr[0] -= (size_t)base;
#ifndef USE_HPVM
      std::cout << "[dc_lib] "
                << " -> " << (void*)sub_ptr[0] << "\n";
#endif
    }
  }
  void to_absolute_pointer(void* base) {
    // to_relative_pointer((void*)(-(long long)base));
    for (int i = 0; i < pointer_recover_list_size; i++) {
      size_t pointer_offset = get_pointer_list_start()[i];
      size_t* sub_ptr = get_pointer_at(pointer_offset);
#ifndef USE_HPVM
      std::cout << "[dc_lib] "
                << "To relative: " << (void*)sub_ptr[0];
#endif
      sub_ptr[0] += (size_t)base;
#ifndef USE_HPVM
      std::cout << "[dc_lib] "
                << " -> " << (void*)sub_ptr[0] << "\n";
#endif
    }
  }
  size_t total_size() {
    return sizeof(*this) + pointer_recover_list_size * sizeof(size_t) +
           obj_size;
  }
};
using AutoDcVectorType = itlib::small_vector<size_t>;
using AutoDcMapType = itlib::flat_map<void*, void*>;
class HpvmBuf {
 public:
  HpvmBufHeader* buf = nullptr;
  AutoDcVectorType rel_pointer_list;
  AutoDcMapType pointer_mapping;
  PinnedVector<byte> obj_data;

  HpvmBuf() {
    rel_pointer_list.reserve(1);
    obj_data.reserve(1);
  }

  HpvmBufHeader* formulate_device_buffer() {
    if (buf) free(buf);
    size_t buf_size = sizeof(HpvmBufHeader) +
                      rel_pointer_list.size() * sizeof(size_t) +
                      obj_data.size();
    buf = (HpvmBufHeader*)calloc(buf_size, 1);
    buf->pointer_recover_list_size = rel_pointer_list.size();
    buf->obj_size = obj_data.size();

    std::cout << "[dc_lib] "
              << "Formulating buffer of " << buf_size << " bytes\n";
    memcpy(buf->get_pointer_list_start(), rel_pointer_list.data(),
           buf->pointer_recover_list_size * sizeof(size_t));
    memcpy(buf->get_obj_start(), obj_data.data(), buf->obj_size);
    std::cout << "[dc_lib] "
              << "We will have pointers at offsets: ";
    for (auto i : rel_pointer_list) std::cout << (void*)i << ",";
    std::cout << "\n";

    buf->to_relative_pointer(obj_data.data());
    return buf;
  }
  template <class T = void>
  T* recover_host_accessible_obj() {
    buf->to_absolute_pointer(buf->get_obj_start());
    return (T*)buf->get_obj_start();
  }

  template <class AllocType = void>
  AllocType* allocate(size_t size) {
    if (size == 0) return nullptr;
    size_t old_size = obj_data.size();
    size_t alloc_size = size + size % 16;  // Align to 16 byte

    obj_data.resize(obj_data.size() + alloc_size);
    std::cout << "[dc_lib] "
              << "Allocate up to:" << obj_data.size() << " bytes" << std::endl;
    return (AllocType*)(obj_data.data() + old_size);
  }

  template <class T>
  void register_pointer_in_buf(T** ptr_addr_in_buf, T* ptr_val_on_host) {
    pointer_mapping[ptr_val_on_host] = *ptr_addr_in_buf;
    rel_pointer_list.push_back((size_t)(ptr_addr_in_buf) -
                               (size_t)obj_data.data());
  }
};

// Main user entry to snapshot an object into a buffer
template <class T>
__attribute__((noinline)) HpvmBuf hpvm_do_snapshot(T* original_obj) {
  std::cout << "[dc_lib] "
            << "Snapshot: " << (void*)original_obj << std::endl;
  HpvmBuf buf;
  hpvm_snapshot_internal(buf, original_obj);
  std::cout << "[dc_lib] "
            << "Snapshot Done\n";
  buf.formulate_device_buffer();
  return buf;
}

// Allocated space at end of buffer and do trivial copy
template <class T>
T* snapshot_trivial(HpvmBuf& buf, T* src, size_t arr_size = 1) {
  auto* dst = buf.template allocate<T>(sizeof(T) * arr_size);
  memcpy(dst, src, sizeof(T) * arr_size);
  return dst;
}

// User can implement this specialization for deep copy
// template <typename ... Types>
// void callCustomOnAll(my_list<Types...>);

// SFINAE test
template <typename T>
class has_custom_behavior {
  typedef char yes_type;
  typedef long no_type;
  template <typename U>
  static yes_type test(decltype(&U::hpvm_snapshot_custom_behavior));
  template <typename U>
  static no_type test(...);

 public:
  static constexpr bool value = sizeof(test<T>(0)) == sizeof(yes_type);
};

template <class T>
extern typename std::enable_if<has_custom_behavior<T>::value>::type
hpvm_snapshot_custom(HpvmBuf& buf, T* dst_obj, T* original_obj) {
  original_obj->hpvm_snapshot_custom_behavior(buf, dst_obj, original_obj);
  // callCustomOnAll(std::__direct_bases<T>::direct_bases);
}
template <class T>
extern typename std::enable_if<!has_custom_behavior<T>::value>::type
hpvm_snapshot_custom(HpvmBuf& buf, T* dst_obj, T* original_obj) {}

// Snapshot function used internally
template <class T>
extern T* hpvm_snapshot_internal(HpvmBuf& buf, T* original_obj) {
  auto* dst = snapshot_trivial(buf, original_obj);
  buf.pointer_mapping[original_obj] = dst;
  hpvm_snapshot_custom<T>(buf, dst, original_obj);
  return dst;
}

// Snapshot an (array of) pointer(s)
template <class T>
void snapshot_pointer(HpvmBuf& buf, T*& dst_ptr, T* src_ptr) {
  if (int num_allocation = allocation_size(src_ptr) / sizeof(T)) {
    std::cout << "[dc_lib] "
              << "Snapshot pointer to buffer of " << num_allocation
              << " elems\n";
    if (buf.pointer_mapping.count(src_ptr)) {
      std::cout << "[dc_lib] "
                << "Loop back!\n";
      dst_ptr = (T*)buf.pointer_mapping[src_ptr];
      buf.register_pointer_in_buf(&dst_ptr, src_ptr);
    } else {
      // Ensure array allocation is continuous
      dst_ptr = snapshot_trivial(buf, src_ptr, num_allocation);
      buf.register_pointer_in_buf(&dst_ptr, src_ptr);

      // Only do deep copy after continuous buffer have been allocated
      for (int i = 0; i < num_allocation; i++)
        hpvm_snapshot_custom<T>(buf, dst_ptr + i, src_ptr + i);
    }
  } else
    dst_ptr = nullptr;
}

// Helper for frontend codegen
template <class T>
void dispatch_snapshot(HpvmBuf& buf, T& dst_field, T& src_field) {
  if (std::is_pointer<T>::value)
    snapshot_pointer(buf, dst_field, src_field);
  else if (std::is_class<T>::value)
    hpvm_snapshot_custom(buf, &dst_field, &src_field);
}

// Device side helper for object recovery
template <class T = void>
T* use_on_device(HpvmBufHeader* buf_header) {
  buf_header->to_absolute_pointer(buf_header->get_obj_start());
  return buf_header->get_obj_start<T>();
}
template <class T = void>
T* use_on_device(HpvmBuf& buf) {
  return use_on_device<T>(buf.buf);
}
template <class T = void, class B>
T* remap_for_use(B b) {
  return use_on_device<T>(b);
}
template <class... Args>
void remap_for_use(Args&&... args) {
  [](...) {}((use_on_device(std::forward<Args>(args)), 0)...);
}

void done_use_on_device(HpvmBufHeader* buf_header) {
  buf_header->to_relative_pointer(buf_header->get_obj_start());
}
void done_use_on_device(HpvmBuf& buf) { done_use_on_device(buf.buf); }

template <class... Args>
void remap_for_copy(Args&&... args) {
  [](...) {}((done_use_on_device(std::forward<Args>(args)), 0)...);
}

extern "C" {
__attribute__((optnone)) extern void hpvm_dc_ignore();
}

// Self type: https://stackoverflow.com/a/70701479
namespace SelfType {
template <typename T>
struct Reader {
  friend auto adl_GetSelfType(Reader<T>);
};

template <typename T, typename U>
struct Writer {
  friend auto adl_GetSelfType(Reader<T>) { return U{}; }
};

inline void adl_GetSelfType() {}

template <typename T>
using Read = std::remove_pointer_t<decltype(adl_GetSelfType(Reader<T>{}))>;
}  // namespace SelfType

// Provide allocator for custom use, e.g container libraries
template <class T>
class DeepCopyAlloc {
 public:
  // type definitions
  typedef T value_type;
  typedef T* pointer;
  typedef const T* const_pointer;
  typedef T& reference;
  typedef const T& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  // rebind allocator to type U
  template <class U>
  struct rebind {
    typedef DeepCopyAlloc<U> other;
  };

  // return address of values
  pointer address(reference value) const { return &value; }
  const_pointer address(const_reference value) const { return &value; }

  /* constructors and destructor
   * - nothing to do because the allocator has no state
   */
  DeepCopyAlloc() throw() {}
  DeepCopyAlloc(const DeepCopyAlloc&) throw() {}
  template <class U>
  DeepCopyAlloc(const DeepCopyAlloc<U>&) throw() {}
  ~DeepCopyAlloc() throw() {}

  // return maximum number of elements that can be allocated
  size_type max_size() const throw() {
    return std::numeric_limits<std::size_t>::max() / sizeof(T);
  }

  // allocate but don't initialize num elements of type T
  pointer allocate(size_type num, const void* = 0) {
    std::cout << "Custom Allocator alloc!\n";
    return hpvm_new_arr<T>(num);
  }

  // deallocate storage p of deleted elements
  void deallocate(pointer p, size_type num) {
    std::cout << "Custom Allocator free!\n";
    hpvm_delete_arr(p);
  }
};

// return that all specializations of this allocator are interchangeable
template <class T1, class T2>
bool operator==(const DeepCopyAlloc<T1>&, const DeepCopyAlloc<T2>&) throw() {
  return true;
}
template <class T1, class T2>
bool operator!=(const DeepCopyAlloc<T1>&, const DeepCopyAlloc<T2>&) throw() {
  return false;
}

// Library function
// ends----------------------------------------------------------
