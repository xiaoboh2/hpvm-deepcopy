#define USE_HPVM
#ifdef USE_HPVM
#include "heterocc.h"
#endif
#include "hpvm_dclib.hpp"
// Make sure dclib is at front of everything

#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <vector>

using namespace std;
// A linked list:
class LNode {
 public:
  size_t val;
  int* other_vals = nullptr;  // arbitrary long array

  LNode* next = nullptr;

  inline LNode(int v) {
    val = v;
    other_vals = hpvm_new_arr<int>(5);
    for (int i = 0; i < 5; i++) other_vals[i] = i + 1 + v;
  }

  static LNode* create_nodes(initializer_list<int> vs) {
    LNode *root = nullptr, *curr = nullptr;
    for (auto v : vs) {
      if (!root)
        root = curr = hpvm_new<LNode>(v);
      else
        curr = curr->next = hpvm_new<LNode>(v);
    }
    return root;
  }

  void delta(int x) {
    for (int i = 0; i < 5; i++) other_vals[i] += x;
  }

  void print() {
    cout << "[Node " << val << ": ";
    for (int i = 0; i < 5; i++) cout << other_vals[i] << ",";
    cout << "]\n -> ";

    if (next)
      next->print();
    else
      cout << "null"
           << "\n";
  }
};

// A structure behave the same as LNode, but without the user-provided copy
// function
struct NonUserCopyStruct : LNode {
 public:
  LNode* node = nullptr;
  using LNode::LNode;
};

void linked_list_kernel(HpvmBufHeader* buf_header, size_t header_size) {
  void* Section = __hetero_section_begin();
  void* Wrapper = __hetero_task_begin(1, buf_header, header_size, 1, buf_header,
                                      header_size);
  __hpvm__hint(hpvm::GPU_TARGET);
  auto* root = use_begin<LNode>(buf_header); // remap for use

  auto* curr = root;
  while (curr) {
    curr->delta(1);  // increment all values in node by 1
    curr = curr->next;
  }

  use_end(buf_header); // remap for copy
  __hetero_task_end(Wrapper);
  __hetero_section_end(Section);
}

int main(int argc, char* argv[]) {
  auto* root = LNode::create_nodes({1, 2, 3, 4, 5, 6, 7, 8});
  cout << "Before kernel:\n";
  root->print();

  cout << "Kernel launch!\n";
  auto host_buf = hpvm_do_snapshot(root);
  void* DFG = __hetero_launch((void*)linked_list_kernel, 1, host_buf.buf,
                              host_buf.buf->total_size(), 1, host_buf.buf,
                              host_buf.buf->total_size());
  __hetero_wait(DFG);

  root = use_begin<LNode>(host_buf);
  cout << "After kernel in buffer:\n";
  root->print();

  return 0;
}

/*
 

 */
