#define USE_HPVM
#ifdef USE_HPVM
#include "heterocc.h"
#endif
#include "hpvm_dclib.hpp"
// Make sure dclib is at front of everything

#define EIGEN_NO_DEBUG  // necessary to prevent unreachable in eigen

#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "./eigen/Eigen/Dense"

// User code

using namespace Eigen;

#ifdef USE_HPVM
// clang-format off
void stencil(HpvmBufHeader* m1bh, size_t m1bh_size, 
             HpvmBufHeader* m2bh, size_t m2bh_size, 
             HpvmBufHeader* m3bh, size_t m3bh_size) {
  // clang-format on
  void* Section = __hetero_section_begin();

  // clang-format off
  void* Wrapper = __hetero_task_begin(3, 
                                      m1bh, m1bh_size, 
                                      m2bh, m2bh_size, 
                                      m3bh, m3bh_size, 
                                      3, 
                                      m1bh, m1bh_size, 
                                      m2bh, m2bh_size, 
                                      m3bh, m3bh_size);
  // clang-format on
  __hpvm__hint(hpvm::GPU_TARGET);

  auto& mat1d = *use_on_device<MatrixXd>(m1bh);
  auto& mat2d = *use_on_device<MatrixXd>(m2bh);
  auto& mat3d = *use_on_device<MatrixXd>(m3bh);
  // auto& mat3d = *use_on_device<Matrix<MatrixXd, Dynamic, Dynamic>>(m3bh);

  // mat3d.noalias() = mat1d * mat2d;
  // mat1d.transposeInPlace();
  mat2d(0, 1) = 999;
  // mat3d(0, 1)(0,1) = 999;

  done_use_on_device(m1bh);
  done_use_on_device(m2bh);
  done_use_on_device(m3bh);

  __hetero_task_end(Wrapper);
  __hetero_section_end(Section);
}
#endif

int main(int argc, char* argv[]) {
  MatrixXd mat1(2, 5);
  mat1.row(0) << 1, 2, 3, 4, 5;
  mat1.row(1) << 5, 4, 3, 2, 1;
  MatrixXd mat2(mat1.transpose());
  MatrixXd mat3(2, 2);
  // Matrix<MatrixXd, Dynamic, Dynamic> mat3(2, 2);
  //mat3.row(0) << mat1, mat1, mat1;
  std::cout << "matrix1:\n" << mat1 << "\n";
  std::cout << "matrix2:\n" << mat2 << "\n";
  std::cout << "host product:\n" << mat1 * mat2 << "\n";

  auto m1b = hpvm_do_snapshot(&mat1);
  auto m2b = hpvm_do_snapshot(&mat2);
  auto m3b = hpvm_do_snapshot(&mat3);

#ifdef USE_HPVM
  std::cout << "Kernel launch!" << std::endl;
  // clang-format off
  void* DFG =
      __hetero_launch((void*)stencil, 3, 
                      m1b.buf, m1b.buf->total_size(),
                      m2b.buf, m2b.buf->total_size(), 
                      m3b.buf, m3b.buf->total_size(), 
                      3,
                      m1b.buf, m1b.buf->total_size(),
                      m2b.buf, m2b.buf->total_size(), 
                      m3b.buf, m3b.buf->total_size());
  // clang-format on
  __hetero_wait(DFG);
#else
  auto& mat1d = *use_on_device<MatrixXd>(m1b.buf);
  auto& mat2d = *use_on_device<MatrixXd>(m2b.buf);
  auto& mat3d = *use_on_device<MatrixXd>(m3b.buf);
  // auto& mat3d = *use_on_device<Matrix<MatrixXd, Dynamic, Dynamic> >(m3b.buf);

  // mat3d.noalias() = mat1d * mat2d;
  // mat3d(0, 1)(0, 1) = 999;
  mat2d(0, 1) = 999;

  done_use_on_device(m1b.buf);
  done_use_on_device(m2b.buf);
  done_use_on_device(m3b.buf);
#endif

  std::cout << "After kernel in buffer:\n";
  auto& recovered_m1 = *m1b.recover_host_accessible_obj<MatrixXd>();
  auto& recovered_m2 = *m2b.recover_host_accessible_obj<MatrixXd>();
  auto& recovered_m3 = *m3b.recover_host_accessible_obj<MatrixXd>();
  std::cout << "device mat1:\n" << recovered_m1 << "\n";
  std::cout << "device mat2:\n" << recovered_m2 << "\n";
  //std::cout << "device product:\n" << recovered_m3 << "\n";

  return 0;
}
