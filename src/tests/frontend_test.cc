//#include <iostream>

#include "hpvm_dclib.hpp"

// template <class T>
class A;

template <class T>
class Tpl {
  T x;
};

template <class T2>
class Tpl2 : Tpl<T2> {
  T2 x2;
};

// template <class T>
class A {
 public:
  int x;
  char* y = nullptr;
};

class B : A {
 public:
  int x;
  A ac;
  A ai;
  Tpl<int> t;
  Tpl2<int> t2;
};

class Unrelated {
#define DEFINE_SELF                                                    \
  struct _self_type_tag {};                                            \
  constexpr auto _self_type_helper()->decltype(                        \
      ::SelfType::Writer<_self_type_tag, decltype(this)>{}, void()) {} \
  using Self = ::SelfType::Read<_self_type_tag>
  DEFINE_SELF;

 public:
  inline void hpvm_snapshot_custom_behavior(HpvmBuf& buf, Self* dst,
                                                   Self* src) {}
};
class Unrelated2 {};

// int hpvm_do_snapshot(B* b){return 0;}
int main() {
  // std::cout << "Test!\n";
  auto buffer = hpvm_do_snapshot(new B());
  //static_assert(has_custom_behavior<Unrelated2>::value);
  return 0;
}
