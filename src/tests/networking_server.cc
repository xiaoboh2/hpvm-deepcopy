#define USE_HPVM
#ifdef USE_HPVM
#include "heterocc.h"
#endif
#include "hpvm_dclib.hpp"
// Make sure dclib is at front of everything

#define EIGEN_NO_DEBUG  // necessary to prevent unreachable in eigen

#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "./eigen/Eigen/Dense"
#include "base64.hpp"

using namespace Eigen;
using namespace std;
// A linked list:
class LNode {
 public:
  size_t val;
  int* other_vals = nullptr;  // arbitrary long array

  LNode* next = nullptr;

  inline LNode(int v) {
    val = v;
    other_vals = hpvm_new_arr<int>(5);
    for (int i = 0; i < 5; i++) other_vals[i] = i + 1 + v;
  }

  static LNode* create_nodes(initializer_list<int> vs) {
    LNode *root = nullptr, *curr = nullptr;
    for (auto v : vs) {
      if (!root)
        root = curr = hpvm_new<LNode>(v);
      else
        curr = curr->next = hpvm_new<LNode>(v);
    }
    return root;
  }

  void delta(int x) {
    for (int i = 0; i < 5; i++) other_vals[i] += x;
  }

  virtual void print() {
    cout << "[Node " << val << ": ";
    for (int i = 0; i < 5; i++) cout << other_vals[i] << ",";
    cout << "]\n -> ";

    if (next)
      next->print();
    else
      cout << "null"
           << "\n";
  }
};

void dummy(void* _, size_t __) {
  void* Section = __hetero_section_begin();
  void* Wrapper = __hetero_task_begin(1, _, __, 1, _, __);
  __hetero_task_end(Wrapper);
  __hetero_section_end(Section);
}

char* print_serialization(HpvmBuf& host_buf) {
  size_t output_len = 0;
  char* b64_buf = base64_encode((unsigned char*)host_buf.buf,
                                host_buf.buf->total_size(), output_len);
  for (size_t i = 0; i < output_len; i++) cout << b64_buf[i];
  cout << endl;
  return b64_buf;
}

template <class T>
T* verify_deserialization(HpvmBuf& host_buf) {
  cout << "Verification input decode to the same buffer: ";
  string inp;
  cin >> inp;
  size_t decoded_len = 0;
  char* decoded =
      (char*)base64_decode((const char*)inp.c_str(), inp.size(), decoded_len);

  // Verification
  for (size_t i = 0; i < host_buf.buf->total_size(); i++)
    if (decoded[i] != ((char*)host_buf.buf)[i])
      cout << i << ": " << (int)((char*)host_buf.buf)[i]
           << " != " << (int)decoded[i] << "\n";

  if (host_buf.buf->total_size() != decoded_len)
    cout << "Decoded into wrong length! " << host_buf.buf->total_size()
         << " != " << decoded_len << "\n";

  // Recover object
  HpvmBuf client_buf;
  client_buf.buf = (HpvmBufHeader*)decoded;
  auto* client_obj = remap_for_use<T>(client_buf);
  return client_obj;
}

int main(int argc, char* argv[]) {
  auto* root = LNode::create_nodes({1, 2, 3, 4, 5, 6, 7, 8});
  cout << "Create nodes on server:\n";
  root->print();
  cout << "Size: " << sizeof(LNode) << "\n";

  cout << "Serialized node definition:\n";
  auto host_buf = hpvm_do_snapshot(root);
  print_serialization(host_buf);

  cout << "Verification input decode to the same buffer: ";
  auto* client_node = verify_deserialization<LNode>(host_buf);
  client_node->print();

  cout << "Serialize a Eigen matrix!\n";
  MatrixXd mat1(2, 5);
  mat1.row(0) << 1, 2, 3, 4, 5;
  mat1.row(1) << 5, 4, 3, 2, 1;
  auto m1b = hpvm_do_snapshot(&mat1);
  cout << "Reference Matrix:\n" << mat1 << "\n";
  print_serialization(m1b);

  cout << "Verification input decode to the same buffer: ";
  auto& m1_rec = *verify_deserialization<MatrixXd>(m1b);
  cout << "Recovered Matrix:\n" << m1_rec << "\n";

  // make hpvm compiler happy
  void* DFG = __hetero_launch((void*)dummy, 1, root, 0, 1, root, 0);
  __hetero_wait(DFG);
  return 0;
}
